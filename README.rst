Test String Contain GeneFlow App
================================

Version: 0.1

This GeneFlow app is used for testing GeneFlow app functionality. Specifically, it's used to test the "str_contain" conditional functionality. 

Inputs
------

1. input: dummy input file.

Parameters
----------

1. super_string: Super string, which can contain sub_string.

2. sub_string: Sub string, which can be part of super_string.
 
3. output: Output text file with result.

